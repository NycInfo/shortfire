import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { filter, Subject, take, takeUntil } from 'rxjs';
import { LoginService} from './login.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit ,OnDestroy{
  isVerify:Boolean = true;
  showValidOtp:boolean = false;
  isforgot:Boolean = true;
  isuser:boolean = true;
  public loginValid = true;
  public username = '';
  public password = '';
  public enteredotp = '';
  showOTP:boolean = false;
  private _destroySub$ = new Subject<void>();
  private readonly returnUrl: string;
  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _authService: AuthService,
    private loginser:LoginService) { this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/game';}

  public ngOnInit(): void {
    this._authService.isAuthenticated$.pipe(
      filter((isAuthenticated: boolean) => isAuthenticated),
      takeUntil(this._destroySub$)
    ).subscribe( _ => this._router.navigateByUrl(this.returnUrl));
  }
  
  public ngOnDestroy(): void {
    this._destroySub$.next();
  }
  public onSubmit(): void {
    if(this.username == "admin@gmail.com" && this.password == "admins"){
      this.showOTP = true;
      this.isVerify = false;
      this.isforgot = false;
      this.isuser = false;
  }
}
  verifyuser(){
  if(this.enteredotp == "123456"){
    this.loginser.getData().subscribe(data=>{
      console.log(data);
    })
  this._router.navigateByUrl('/pincode');
  }
  else{
    this.showValidOtp = true;
  }
}


    // this._authService.login(this.username, this.password).pipe(
    //   take(1)
    // ).subscribe({
    //   next: _ => {
    //     this.loginValid = true;
    //     this._router.navigateByUrl('/game');
    //   },
    //   error: _ => this.loginValid = false
    // });
  }