import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { filter, Subject, take, takeUntil } from 'rxjs';
import { ForgotpasswordService } from './forgotpassword.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit ,OnDestroy{
  public username = '';
  isReseted:boolean = false;
  isRequired:boolean = false;
  private _destroySub$ = new Subject<void>();
  private readonly returnUrl: string;

  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _authService: AuthService,
    private forgotuser: ForgotpasswordService
    ) { this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/game';}

  public ngOnInit(): void {
    this._authService.isAuthenticated$.pipe(
      filter((isAuthenticated: boolean) => isAuthenticated),
      takeUntil(this._destroySub$)
    ).subscribe( _ => this._router.navigateByUrl(this.returnUrl));
  }
  public ngOnDestroy(): void {
    this._destroySub$.next();
  }
  forgotverifyuser(){
    if(this.username == "admin@gmail.com"){
      this.forgotuser.getData().subscribe(data=>{
        console.log(data);
        // this.isReseted = true;
    });
}
}  
}
