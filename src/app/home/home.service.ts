import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  
  constructor(private http:HttpClient) { }
  getData(){
    let apiurl = 'https://jsonplaceholder.typicode.com/posts'
    return this.http.get(apiurl)
  }
}
