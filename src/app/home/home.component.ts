import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { filter, Subject, take, takeUntil } from 'rxjs';
import { HomeService } from './home.service'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  showFiller = false;
  showDashboard = false;
  showShortfilms = false;
  showMostViewed = false;
  showMostLiked = false;
  showUsers = false;
  showComments = false;
  showRole = false;
  userdata = [
              {sno:"1",firstname:"Vijaya",lastname:"Thoutam",email:"vijaya@gmail.com",mobile:"9989250688",      dob:"1995-03-07",role:"SEO"},
              {sno:"2",firstname:"Chandana",lastname:"Thoutam",email:"chanduthoutam@gmail.com",mobile:"9505670932",dob:"1997-12-15",role:"SEO"},
              {sno:"3",firstname:"Lakshmi",lastname:"Nadikattu",email:"lakshmi6296@gmail.com",mobile:"8309647156",dob:"1991-05-29",role:"User"},
              {sno:"1",firstname:"Siri",lastname:"Thoutam",email:"sirithoutam@gmail.com",mobile:"9542669744",dob:"1995-08-19",role:"Admin"}
            ];
  private _destroySub$ = new Subject<void>();
  private readonly returnUrl: string;
  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _authService: AuthService,
    private homeuser:HomeService) { this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/game';}

  public ngOnInit(): void {
    this._authService.isAuthenticated$.pipe(
      filter((isAuthenticated: boolean) => isAuthenticated),
      takeUntil(this._destroySub$)
    ).subscribe( _ => this._router.navigateByUrl(this.returnUrl));
  }
  menubtn(){
    this.showFiller = true;
    this.homeuser.getData().subscribe(data=>{
      console.log(data);
    })
  }
  dashclick(){
    this.showDashboard = true;
    this.showShortfilms = false;
    this.showMostViewed = false;
    this.showMostLiked = false;
    this.showUsers = false;
    this.showComments = false;
    this.showRole = false;
  }
  shortfilmclick(){
    this.showShortfilms = true;
    this.showDashboard = false;
    this.showMostViewed = false;
    this.showMostLiked = false;
    this.showUsers = false;
    this.showComments = false;
    this.showRole = false;
  }
  mostviewedsf(){
    this.showMostViewed = true;
    this.showShortfilms = false;
    this.showDashboard = false;
    this.showMostLiked = false;
    this.showUsers = false;
    this.showComments = false;
    this.showRole = false;
  }
  mostlikedsf(){
    this.showMostLiked = true;
    this.showMostViewed = false;
    this.showShortfilms = false;
    this.showDashboard = false;
    this.showUsers = false;
    this.showComments = false;
    this.showRole = false;
  }
  usersclick(){
    this.showUsers = true;
    this.showMostLiked = false;
    this.showMostViewed = false;
    this.showShortfilms = false;
    this.showDashboard = false;
    this.showComments = false;
    this.showRole = false;
  }
  commentsclick(){
    this.showComments = true;
    this.showUsers = false;
    this.showMostLiked = false;
    this.showMostViewed = false;
    this.showShortfilms = false;
    this.showDashboard = false;
    this.showRole = false;
  }
  rolesclick(){
    this.showRole = true;
    this.showComments = false;
    this.showUsers = false;
    this.showMostLiked = false;
    this.showMostViewed = false;
    this.showShortfilms = false;
    this.showDashboard = false;
  }
}
