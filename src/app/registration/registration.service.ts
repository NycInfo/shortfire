import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http:HttpClient) { }
  getData(){
    let apiurl = 'https://jsonplaceholder.typicode.com/posts'
    return this.http.get(apiurl)
  }
}
