import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { filter, Subject, take, takeUntil } from 'rxjs';
import { RegistrationService} from './registration.service'

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit ,OnDestroy{
 
  // public regvalid = true;
  public firstname = '';
  public lastname = '';
  public email = '';
  public mobilenumber = '';
  public password = '';
  public confirmpassword = '';
  private _destroySub$ = new Subject<void>();
  private readonly returnUrl: string;

  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _authService: AuthService,
    private registrationuser:RegistrationService) { this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/game';}

  public ngOnInit(): void {
    this._authService.isAuthenticated$.pipe(
      filter((isAuthenticated: boolean) => isAuthenticated),
      takeUntil(this._destroySub$)
    ).subscribe( _ => this._router.navigateByUrl(this.returnUrl));
  }
  
  public ngOnDestroy(): void {
    this._destroySub$.next();
  }
  public onSubmit(): void {
    
}
registerclick(){
  this.registrationuser.getData().subscribe(data=>{
    console.log(data);
  })
}
  


    // this._authService.login(this.username, this.password).pipe(
    //   take(1)
    // ).subscribe({
    //   next: _ => {
    //     this.loginValid = true;
    //     this._router.navigateByUrl('/game');
    //   },
    //   error: _ => this.loginValid = false
    // });
  }

