import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';
import { RegistrationComponent } from './registration/registration.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { PincodeComponent } from './pincode/pincode.component'

const routes: Routes = [
  {
    path: 'game',
    component: CalculatorComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'forgotpassword',
    component: ForgotpasswordComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path:'registration',
    component: RegistrationComponent
  },
  {
    path: 'pincode',
    component: PincodeComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
