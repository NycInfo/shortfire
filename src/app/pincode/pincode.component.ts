import { Component, OnDestroy, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { filter, Subject, take, takeUntil } from 'rxjs';
import { PincodeService } from './pincode.service'
import { MatTableDataSource } from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-pincode',
  templateUrl: './pincode.component.html',
  styleUrls: ['./pincode.component.css']
})
export class PincodeComponent implements OnInit {
  imageSrc = 'assets/iphone.jpg'  
   imageAlt = 'iPhone'
  name!: string;
  branchtype!: string;
  district!: string;
  block!: string;
  state!: string;
  isTable:boolean=false;
  // paginator: any;
  // position: number | undefined;
  // weight: number | undefined;
  // symbol: string | undefined;
  showError:boolean = false;
  pincode = "";
  public dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['Name', 'BranchType', 'District', 'Block','State'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  private _destroySub$ = new Subject<void>();
  private readonly returnUrl: string;
  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _authService: AuthService,
    private pinuser:PincodeService) { this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/game';}
   resultdt : any = [];
  public ngOnInit(): void {
    this._authService.isAuthenticated$.pipe(
      filter((isAuthenticated: boolean) => isAuthenticated),
      takeUntil(this._destroySub$)
    ).subscribe( _ => this._router.navigateByUrl(this.returnUrl));
  }  
  postclick(){
    this.pinuser.getPostDetails(this.pincode).subscribe((data:any)=>{
      console.log(data);
      var postdet = data[0].PostOffice;
      this.dataSource = new MatTableDataSource(postdet);
      console.log(postdet);
            // postdet.forEach((ele:any) => {
      //   this.resultdt.push({"name":ele.Name,"branchtype":ele.BranchType,"district":ele.District,"block":ele.Block,"state":ele.State})
      // });
    });
    // this.dataSource = this.resultdt;
    
    // console.log(this.dataSource);
    this.isTable=true;
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  public ngOnDestroy(): void {
    this._destroySub$.next();
  }
}