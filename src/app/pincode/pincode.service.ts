import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PincodeService {

  constructor(private http:HttpClient) { }
  getPostDetails(pincode: string){
    let apiurl = 'https://api.postalpincode.in/pincode/'+pincode;
    return this.http.get(apiurl)
  }
}
